var RPTToken = artifacts.require("./RPTToken.sol");
var RPTCrowdsale = artifacts.require("./RPTCrowdsale.sol");
var BigNumber = require('bignumber.js');

contract(RPTCrowdsale , function(accounts){
founderMultiSigAddressOne = accounts[1];
reaminingTokenHolder = accounts[7];
owner = accounts[3];
tokenAddress = "0x3e1aa5712fe46c78a803a4c053597f7f8c41de0c";
advisorAddress = accounts[4];
founderMultiSigAddressTwo = accounts[5];
authorizerAddress = accounts[6];


it("intialization of the crowdfund with founder address , remaining token holder address , token conversion rate",function(done){
     var now = Date.now();
     var crowdsale;
    RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        return crowdsale.founderMultiSigAddress.call();
    }).then(function(address){
        assert.strictEqual(address,founderMultiSigAddressOne);
        return true;  
    }).then(function(result){
        return crowdsale.setTokenAddress(tokenAddress,{from: founderMultiSigAddressOne , gas:150000})
    }).then(function(result){
        return crowdsale.startCrowdfund(2000,{from: founderMultiSigAddressOne , gas:150000});
    }).then(function(result){
        return crowdsale.crowdfundEndTime.call();
    }).then(function(result){
        assert.strictEqual(Math.floor(now/1000 + 35*24*60*60), result.toNumber());
        done();
    }).catch(done);
});

// Test cases for setfounderMultiSigAddressOne 

it("setFounderMultiSigAddress : Should change the founder multi signature address",function(done){
    var crowdsale;
     RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        return instance;
    }).then(function(result){
        crowdsale.setFounderMultiSigAddress(accounts[6],{from : founderMultiSigAddressOne , gas:1500000}).then(function(tx){
            return tx;
        }).then(function(result){
            return crowdsale.founderMultiSigAddress.call();
        }).then(function(account){
            assert.strictEqual(account,accounts[6]);
            done();
        }).catch(done);
    });
});

it("setFounderMultiSigAddress : Attempt to change the founder multi signature address by other then owner -- fails",function(){
   var crowdsale;
     RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        return instance;
    }).then(function(result){
        crowdsale.setFounderMultiSigAddress(accounts[6],{from : accounts[7] , gas:1500000}).then(function(tx){
        return tx;
    }).then(function(result){
        assert(false, "it expected to return exception but it didn't");
    }).catch(function(error){
        if(error.toString().indexOf("invalid opcode") != -1) {
         console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to check the call other than owner");
         assert(true,true);
         }else{
         assert(false,error.toString());
        }
    });
  });
});


// // Test cases for setTokenAddress

it("setTokenAddress: should set token address in crowdfund contract", function(done){
     var crowdsale;
     RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        return instance;
     }).then(function(result){
         crowdsale.setTokenAddress(tokenAddress,{from:founderMultiSigAddressOne, gas:150000}).then(function(tx){
             return tx;
         }).then(function(result){
             crowdsale.startCrowdfund(2000,{from:founderMultiSigAddressOne, gas:150000}).then(function(state){
                 return state;
             }).then(function(result){
                 return crowdsale.isCrowdFundActive.call();
             }).then(function(status){
                 assert.isTrue(status);
                 done();
             }).catch(done);
         });
     });
});


it("setTokenAddress : Attempt to set token address in crowdfund contract by another address instead of founder address -- fail ", function(){
     var crowdsale;
     RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        return instance;
     }).then(function(result){
         crowdsale.setTokenAddress(tokenAddress,{from:accounts[8], gas:150000}).then(function(tx){
             return tx;
         }).then(function(result){
            assert(false, "it expected to return exception but it didn't");
    }).catch(function(error){
        if(error.toString().indexOf("invalid opcode") != -1) {
         console.log("We were expecting a Solidity throw (aka an invalid opcode), Test succeeded to check the call other than owner");
         assert(true,true);
         }else{
         assert(false,error.toString());
        }
    });
  });
});

// // // ============================================================================
 
 it("setTokenAddress : Should NOT let a founder address to set the token address when token is set", function(done) {
        var crowdsale;
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
            return crowdsale.setTokenAddress(tokenAddress,{from:founderMultiSigAddressOne, gas:150000});
        }).then(() => {
            return crowdsale.setTokenAddress(tokenAddress,{from:founderMultiSigAddressOne, gas:150000});
        }).catch(() => {
            assert(true, true);
            done();
        });
    });

// // // Test case for changeCrowdfundState

it(" changeCrowdfundState :Should NOT let a random address activate the crowdfund", function(done) {
         var crowdsale;
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
            return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddressOne, gas:150000});
        }).then(function (result) {
            return crowdsale.token.call();
        }).then(function (result) {
            assert.strictEqual(result, tokenAddress);
            return crowdsale.changeCrowdfundState({from: accounts[8], gas:150000});
        }).catch(() => {
            assert(true, true);
            done();
        });
    });



    it("changeCrowdfundState :Not let the founder address to activate the crowdfund if token is not set", function(done) {
       var crowdsale;
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
            return crowdsale.changeCrowdfundState({from: founderMultiSigAddressOne, gas:150000});
         }).catch(() => {
            assert(true, true);
            done();
        });
    });


    it(" changeCrowdfundState :crowdfund should be deactivated by addresss after calling startCrowdFund()", function(done) {
         var crowdsale;
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
            return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddressOne, gas:150000});
        }).then(function (result) {
            return crowdsale.token.call();
        }).then(function (result) {
            assert.strictEqual(result, tokenAddress);
            return crowdsale.startCrowdfund(2000,{from:founderMultiSigAddressOne , gas:150000});
        }).then(function(result){
                return crowdsale.isCrowdFundActive.call();
            }).then(function(result){
                assert.isTrue(result);
                return crowdsale.changeCrowdfundState({from : founderMultiSigAddressOne , gas:150000});
            }).then(function(result){
                return crowdsale.isCrowdFundActive.call();
            }).then(function(result){
                assert.isFalse(result);
            done();
        }).catch(done);
    });


     it(" changeCrowdfundState :crowdfund should be deactivated by addresss before calling startCrowdFund() --fail", function(done) {
         var crowdsale;
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
            return crowdsale.setTokenAddress(tokenAddress, {from:founderMultiSigAddressOne, gas:150000});
        }).then(function (result) {
            return crowdsale.token.call();
        }).then(function (result) {
            return assert.strictEqual(result, tokenAddress);
        }).then(function(result){
            return crowdsale.changeCrowdfundState({from : founderMultiSigAddressOne , gas:150000});
        }).catch(() => {
            assert(true, true);
            done();
        });
    });



    it("buyTokens : buy tokens at presale",function(done){
     var crowdsale;
    var token;
    var now = Date.now();
    initialBalance = web3.eth.getBalance(founderMultiSigAddressOne);
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
         RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
           return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddressOne, gas:150000});
    }).then(function (result) {
                return new Promise((resolve, reject) => {
                    web3.currentProvider.sendAsync({
                        jsonrpc: "2.0",
                        method: "evm_increaseTime",
                        params: [(500)] // 500 seconds after the start of crowdfund 
                    }, function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                    });
                });
    }).then(function(result){
        return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(5).times(new BigNumber(10).pow(18))});
    }).then(function (result) {
        return web3.eth.getBalance(founderMultiSigAddressOne);
    }).then(function (result) {
            assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 5, 0.16);
            return token.balanceOf.call(accounts[8]);
    }).then(function(balance){
            assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 32234.4);  //  bonus 21 %
            return token.totalAllocatedTokens.call();
    }).then(function(result){
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),50032234.4);
         done();  
    }).catch(done);
});
});



it("buyTokens : buy tokens according to bonus schedule -- first week",function(done){
     var crowdsale;
    var token;
    var now = Date.now();
    initialBalance = web3.eth.getBalance(founderMultiSigAddressOne);
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
         RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
           return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddressOne, gas:150000});
    }).then(function(result){
        return crowdsale.startCrowdfund(5920,{from: founderMultiSigAddressOne, gas:150000});
    }).then(function (result) {
                return new Promise((resolve, reject) => {
                    web3.currentProvider.sendAsync({
                        jsonrpc: "2.0",
                        method: "evm_increaseTime",
                        params: [(500)] // 500 seconds after the start of crowdfund 
                    }, function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        resolve(result);
                    });
                });
    }).then(function(result){
        return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(1).times(new BigNumber(10).pow(18))});
    }).then(function (result) {
        return web3.eth.getBalance(founderMultiSigAddressOne);
    }).then(function (result) {
            assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 1, 0.17);
            return token.balanceOf.call(accounts[8]);
    }).then(function(balance){
            assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 7163.2);  // 2 week bonus 21 %
            return token.totalAllocatedTokens.call();
    }).then(function(result){
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),50007163.2);
         done();  
    }).catch(done);
});
});


it("buyTokens : buy tokens according to bonus schedule -- 2nd week",function(done){
    var crowdsale;
    var token;
    var now = Date.now();
    initialBalance = web3.eth.getBalance(founderMultiSigAddressOne);
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
         RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
           return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddressOne, gas:150000});
    }).then(function(result){
        return crowdsale.startCrowdfund(5920,{from: founderMultiSigAddressOne, gas:150000});
    }).then(function(result){
               return new Promise((resolve,reject) => {
                web3.currentProvider.sendAsync({
                jsonrpc: "2.0",
                method: "evm_increaseTime",
                params: [(7*24*60*60 + 200)] // 100 seconds after the first week 
            }, function(err, result) {
                if(err) {
                    reject(err);
                }
                resolve(result);
                });
            });
    }).then(function(result){
        return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:2000000 , value: new BigNumber(6).times(new BigNumber(10).pow(18))});
    }).then(function (result) {
        return web3.eth.getBalance(founderMultiSigAddressOne);
    }).then(function (result) {
            assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 6, 0.17);
            return token.balanceOf.call(accounts[8]);
    }).then(function(balance){
            assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 40492.8);  // 2 week bonus 14 %
            return token.totalAllocatedTokens.call();
    }).then(function(result){
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),50040492.8);
         done();  
    }).catch(done);
});
});



it("buyTokens : buy tokens according to bonus schedule -- 3rd week",function(done){
    var crowdsale;
    var token;
    var now = Date.now();
    initialBalance = web3.eth.getBalance(founderMultiSigAddressOne);
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
           return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddressOne, gas:150000});
        }).then(function(result){
            return crowdsale.startCrowdfund(5920,{from: founderMultiSigAddressOne, gas:150000});
        }).then(function(result){
               return new Promise((resolve,reject) => {
                web3.currentProvider.sendAsync({
                jsonrpc: "2.0",
                method: "evm_increaseTime",
                params: [(14*24*60*60 + 100)] // 100 seconds after the second week 
            }, function(err, result) {
                if(err) {
                    reject(err);
                }
                resolve(result);
                });
            });
        }).then(function(result){
            return crowdsale.buyTokens(accounts[8],{from : accounts[8], gas:200000,value: new BigNumber(7).times(new BigNumber(10).pow(18))});
        }).then(function (result) {
            return web3.eth.getBalance(founderMultiSigAddressOne);
        }).then(function (result) {
            assert.closeTo((result.minus(initialBalance).dividedBy(new BigNumber(10).pow(18))).toNumber(), 7, 0.17);
            return token.balanceOf.call(accounts[8]);
        }).then(function(balance){
            assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(), 44340.8);  // 3 week bonus 7 %
            return token.totalAllocatedTokens.call();
        }).then(function(result){
            assert.strictEqual(result.dividedBy(new BigNumber(10).pow(18)).toNumber(),50044340.8);
            done();
        }).catch(done);    
    });
});



 it("buyTokens: should Not let buy tokens -- crowdfund is done", function(done) {
        var crowdsale;
        var token;
        var now = Date.now();
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instanceTo;
        return token;
        }).then(function (result) {
            return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddressOne, gas:150000});
        }).then(function (result) {
            return crowdsale.changeCrowdfundState({from: founderMultiSigAddressOne});
        }).then(function (result) {
            return new Promise((resolve,reject) => {
                web3.currentProvider.sendAsync({
                jsonrpc: "2.0",
                method: "evm_increaseTime",
                params: [(21*24*60*60 + 200)] // 200 seconds after the crowdfund ended
            }, function(err, result) {
                if(err) {
                    reject(err);
                }
                resolve(result);
                });
            });
        }).then(function(result){
            return crowdsale.buyTokens(accounts[8], {value: new BigNumber(1).times(new BigNumber(10).pow(18)), from: accounts[8]});
        }).catch(function (result) {
            assert(true, true)
            done();
        });
    });
 });



 it("buyTokens: should Not let buy tokens -- no token address", function(done) {
       var crowdsale;
        var token;
        var now = Date.now();
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
         RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
        }).then(function (result) {
            return crowdsale.changeCrowdfundState({from: founderMultiSigAddressOne});
        }).then(function (result) {
            return crowdsale.buyTokens(accounts[8], {value: new BigNumber(1).times(new BigNumber(10).pow(18)), from: accounts[8]});
        }).catch(function (result) {
            assert(true, true)
            done();
        }).catch(done);
    });
 });



it("buyTokens: should Not let buy tokens -- crowdfund is not active", function(done) {
        var crowdsale;
        var token;
        var now = Date.now();
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
        }).then(function (result) {
            return token.setTokenAddress(result.address, {from: founderMultiSigAddressOne});
        }).then(function (result) {
            return crowdsale.buyTokens(accounts[8], {value: new BigNumber(2).times(new BigNumber(10).pow(18)), from: accounts[8]});
        }).catch(function (result) {
            assert(true, true)
            done();
        }).catch(done);
    });
});

it("function(): should throw on an undetermined call", function(done) {
       var crowdsale;
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
            return crowdsale.call();
        }).catch(function (result) {
            assert.equal(true, true);
            done();
        })
    });

//  // Test case to check the functionality of close crowdFund

it("closeCrowdFund : close crowd fund and send tokens to remaining token holder ",function(done){
    var crowdsale;
    var token;
    var intialBalance;
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
        return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddressOne, gas:150000});
    }).then(function(result){
        return crowdsale.startCrowdfund(5920,{from: founderMultiSigAddressOne, gas:150000});
    }).then(function(result){
            return new Promise((resolve,reject) => {
                web3.currentProvider.sendAsync({
                jsonRPT: "2.0",
                method: "evm_increaseTime",
                params: [(200)] // 200 seconds after the crowdfund started
            }, function(err, result) {
                if(err) {
                    reject(err);
                }
                resolve(result);
                });
            });
      }).then(function (result) {
         return crowdsale.buyTokens(accounts[8],{from: accounts[8], gas:2000000 , value: new BigNumber(1).times(new BigNumber(10).pow(18))});
     }).then(function(result){
            return new Promise((resolve,reject) => {
                web3.currentProvider.sendAsync({
                jsonRPT: "2.0",
                method: "evm_increaseTime",
                params: [(21*24*60*60 + 200)] // 200 seconds after the crowdfund ended
            }, function(err, result) {
                if(err) {
                    reject(err);
                }
                resolve(result);
                });
            });
         }).then(function(result){
         return token.balanceOf.call(reaminingTokenHolder);
         }).then(function(balance){
        return assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(),0);    
        }).then(function(result){
             return crowdsale.endCrowdfund({from:founderMultiSigAddressOne , gas:1500000});
        }).then(function(result){
            return token.balanceOf.call(reaminingTokenHolder);
        }).then(function(balance){
           assert.strictEqual(balance.dividedBy(new BigNumber(10).pow(18)).toNumber(),699992836.8); //remaining fund
            done(); 
        }).catch(done);
});
});

it("sentTokensToPresaleSubs : it should sent 50 RPC tokens to subscriber account[8]",function(done){
    var crowdsale;
    var token;
    var now = Date.now();
    initialBalance = web3.eth.getBalance(founderMultiSigAddressOne);
        RPTCrowdsale.new(founderMultiSigAddressOne,reaminingTokenHolder,{from : owner , gas : 2000000}).then(function(instance){
        crowdsale = instance;
        RPTToken.new(crowdsale.address,founderMultiSigAddressOne, advisorAddress, founderMultiSigAddressTwo,{from : founderMultiSigAddressOne , gas:2000000}).then(function(instance){
        token = instance;
        return token;
    }).then(function(result){
           return crowdsale.setTokenAddress(result.address,{from:founderMultiSigAddressOne, gas:150000});
        }).then(function(result){
               return new Promise((resolve,reject) => {
                web3.currentProvider.sendAsync({
                jsonrpc: "2.0",
                method: "evm_increaseTime",
                params: [(100)] // 100 seconds after the start 
            }, function(err, result) {
                if(err) {
                    reject(err);
                }
                resolve(result);
                });
            });
        }).then(function(result){
            return crowdsale.sentTokensToPresaleSubs(accounts[8],{from : founderMultiSigAddressOne, gas:200000});
        }).then(function (result) {
            return token.balanceOf.call(accounts[8]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 50);
            done();
        }).catch(done);    
    });
});


 });