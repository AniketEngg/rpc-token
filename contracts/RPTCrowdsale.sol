pragma solidity ^0.4.11;

import './lib/safeMath.sol';
import './RPTToken.sol';

contract RPTCrowdsale {

    using SafeMath for uint256;
    
    RPTToken public token;                                          // Token variable
    //variables
   
    uint256 public totalWeiRaised;                                  // Flag to track the amount raised
    uint32 public exchangeRate = 3000;                              // calculated using priceOfEtherInUSD/priceOfRPTToken 
    uint256 public preDistriToAcquiantancesStartTime = 1510876801;  // Friday, 17-Nov-17 00:00:01 UTC
    uint256 public preDistriToAcquiantancesEndTime = 1511827199;    // Monday, 27-Nov-17 23:59:59 UTC
    uint256 public presaleStartTime = 1511827200;                   // Tuesday, 28-Nov-17 00:00:00 UTC
    uint256 public presaleEndTime = 1513036799;                     // Monday, 11-Dec-17 23:59:59 UTC
    uint256 public crowdfundStartTime = 1513036800;                 // Tuesday, 12-Dec-17 00:00:00 UTC
    uint256 public crowdfundEndTime = 1515628799;                   // Wednesday, 10-Jan-18 23:59:59 UTC
    bool internal isTokenDeployed = false;                          // Flag to track the token deployment
    
    // addresses
    address public founderMultiSigAddress;                          // Founders multi sign address
    address public remainingTokenHolder;                            // Address to hold the remaining tokens after crowdfund end
    address public beneficiaryAddress;                              // All funds are transferred to this address
    

    enum State { Acquiantances, PreSale, CrowdFund, Closed }

    //events
    event TokenPurchase(address indexed beneficiary, uint256 value, uint256 amount); 
    event CrowdFundClosed(uint256 _blockTimeStamp);
    event ChangeFoundersWalletAddress(uint256 _blockTimeStamp, address indexed _foundersWalletAddress);
   
    //Modifiers
    modifier tokenIsDeployed() {
        require(isTokenDeployed == true);
        _;
    }
     modifier nonZeroEth() {
        require(msg.value > 0);
        _;
    }

    modifier nonZeroAddress(address _to) {
        require(_to != 0x0);
        _;
    }

    modifier onlyFounders() {
        require(msg.sender == founderMultiSigAddress);
        _;
    }

    modifier onlyPublic() {
        require(msg.sender != founderMultiSigAddress);
        _;
    }

    modifier inState(State state) {
        require(getState() == state); 
        _;
    }

    modifier inBetween() {
        require(now >= preDistriToAcquiantancesStartTime && now <= crowdfundEndTime);
        _;
    }

    // Constructor to initialize the local variables 
    function RPTCrowdsale (address _founderWalletAddress, address _remainingTokenHolder, address _beneficiaryAddress) {
        founderMultiSigAddress = _founderWalletAddress;
        remainingTokenHolder = _remainingTokenHolder;
        beneficiaryAddress = _beneficiaryAddress;
    }

    // Function to change the founders multi sign address 
     function setFounderMultiSigAddress(address _newFounderAddress) onlyFounders  nonZeroAddress(_newFounderAddress) {
        founderMultiSigAddress = _newFounderAddress;
        ChangeFoundersWalletAddress(now, founderMultiSigAddress);
    }
    
    // Attach the token contract     
    function setTokenAddress(address _tokenAddress) external onlyFounders nonZeroAddress(_tokenAddress) {
        require(isTokenDeployed == false);
        token = RPTToken(_tokenAddress);
        isTokenDeployed = true;
    }


    // function call after crowdFundEndTime it transfers the remaining tokens to remainingTokenHolder address
    function endCrowdfund() onlyFounders returns (bool) {
        require(now > crowdfundEndTime);
        uint256 remainingToken = token.balanceOf(this);  // remaining tokens

        if (remainingToken != 0) {
          token.transfer(remainingTokenHolder, remainingToken); 
          CrowdFundClosed(now);
          return true; 
        } else {
            CrowdFundClosed(now);
            return false;
        }
       
    }

    // Buy token function call only in duration of crowdfund active 
    function buyTokens(address beneficiary)
    nonZeroEth 
    tokenIsDeployed 
    onlyPublic 
    nonZeroAddress(beneficiary) 
    inBetween
    payable 
    public 
    returns(bool) 
    {
            fundTransfer(msg.value);

            uint256 amount = getNoOfTokens(exchangeRate, msg.value);
            
            if (token.transfer(beneficiary, amount)) {
                token.changeTotalSupply(amount); 
                totalWeiRaised = totalWeiRaised.add(msg.value);
                TokenPurchase(beneficiary, msg.value, amount);
                return true;
            } 
            return false;
        
    }


    // function to transfer the funds to founders account
    function fundTransfer(uint256 weiAmount) internal {
        beneficiaryAddress.transfer(weiAmount);
    }

// Get functions 

    // function to get the current state of the crowdsale
    function getState() internal constant returns(State) {
        if (now >= preDistriToAcquiantancesStartTime && now <= preDistriToAcquiantancesEndTime) {
            return State.Acquiantances;
        } if (now >= presaleStartTime && now <= presaleEndTime) {
            return State.PreSale;
        } if (now >= crowdfundStartTime && now <= crowdfundEndTime) {
            return State.CrowdFund;
        } else {
            return State.Closed;
        }
        
    }


   // function to calculate the total no of tokens with bonus multiplication
    function getNoOfTokens(uint32 _exchangeRate, uint256 _amount) internal returns (uint256) {
         uint256 noOfToken = _amount.mul(uint256(_exchangeRate));
         uint256 noOfTokenWithBonus = ((uint256(100 + getCurrentBonusRate())).mul(noOfToken)).div(100);
         return noOfTokenWithBonus;
    }

    

    // function provide the current bonus rate
    function getCurrentBonusRate() internal returns (uint8) {
        
        if (getState() == State.Acquiantances) {
            return 40;
        }
        if (getState() == State.PreSale) {
            return 20;
        }
        if (getState() == State.CrowdFund) {
            return 0;
        }
        else{
        return 0;
        }
    }

    // provides the bonus % 
    function getBonus() constant returns (uint8) {
        return getCurrentBonusRate();
    }

    // send ether to the contract address
    // With at least 200 000 gas
    function() public payable {
        buyTokens(msg.sender);
    }
}

