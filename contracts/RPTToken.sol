pragma solidity ^0.4.11;

import './helpers/BasicToken.sol';
import './lib/safeMath.sol';

contract RPTToken is BasicToken {

using SafeMath for uint256;

string public name = "RPT Token";                  //name of the token
string public symbol = "RPT";                      // symbol of the token
uint8 public decimals = 18;                        // decimals
uint256 public totalSupply = 1000000000 * 10**18;  // total supply of RPT Tokens  

// variables
uint256 public keyEmployeeAllocation;               // fund allocated to key employee
uint256 public totalAllocatedTokens;                // variable to regulate the funds allocation
uint256 public tokensAllocatedToCrowdFund;          // funds allocated to crowdfund

// addresses
address public founderMultiSigAddress = 0xf96E905091d38ca25e06C014fE67b5CA939eE83D;    // multi sign address of founders which hold 
address public crowdFundAddress;                    // address of crowdfund contract

//events
event ChangeFoundersWalletAddress(uint256  _blockTimeStamp, address indexed _foundersWalletAddress);
event TransferPreAllocatedFunds(uint256  _blockTimeStamp , address _to , uint256 _value);

//modifiers
  modifier onlyCrowdFundAddress() {
    require(msg.sender == crowdFundAddress);
    _;
  }

  modifier nonZeroAddress(address _to) {
    require(_to != 0x0);
    _;
  }

  modifier onlyFounders() {
    require(msg.sender == founderMultiSigAddress);
    _;
  }

   // creation of the token contract 
   function RPTToken (address _crowdFundAddress) {
    crowdFundAddress = _crowdFundAddress;

    // Token Distribution  
    tokensAllocatedToCrowdFund = 70 * 10 ** 25;        // 70 % allocation of totalSupply
    keyEmployeeAllocation = 30 * 10 ** 25;             // 30 % allocation of totalSupply

    // Assigned balances to respective stakeholders
    balances[founderMultiSigAddress] = keyEmployeeAllocation;
    balances[crowdFundAddress] = tokensAllocatedToCrowdFund;

    totalAllocatedTokens = balances[founderMultiSigAddress];
  }

// function to keep track of the total token allocation
  function changeTotalSupply(uint256 _amount) onlyCrowdFundAddress {
    totalAllocatedTokens += _amount;
  }

// function to change founder multisig wallet address            
  function changeFounderMultiSigAddress(address _newFounderMultiSigAddress) onlyFounders nonZeroAddress(_newFounderMultiSigAddress) {
    founderMultiSigAddress = _newFounderMultiSigAddress;
    ChangeFoundersWalletAddress(now, founderMultiSigAddress);
  }
 

}
